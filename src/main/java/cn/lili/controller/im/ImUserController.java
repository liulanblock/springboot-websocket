package cn.lili.controller.im;


import cn.lili.modules.im.entity.ImUser;
import cn.lili.modules.im.service.ImUserService;
import cn.lili.security.AuthUser;
import cn.lili.security.UserContext;
import cn.lili.result.*;
import lombok.RequiredArgsConstructor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Chopper
 */
@RestController
@Api(tags = "Im消息接口")
@RequestMapping("/lili/imUser")
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ImUserController {

    private final ImUserService imUserService;

    @GetMapping
    @ApiOperation(value = "获取用户信息")
    public ResultMessage<ImUser> get() {
        AuthUser authUser = UserContext.getAuthUser();
        return ResultUtil.data(imUserService.getById(UserContext.getAuthUser().getId()));
    }

}
