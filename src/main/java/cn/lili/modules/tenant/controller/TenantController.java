package cn.lili.modules.tenant.controller;

import cn.lili.modules.tenant.entity.Tenant;
import cn.lili.modules.tenant.service.TenantService;
import cn.lili.mybatis.util.PageUtil;
import cn.lili.result.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author Chopper
 */
@RestController
@Api(tags = "租户接口")
@RequestMapping("/lili/tenant")
public class TenantController {

    @Autowired
    private TenantService tenantService;

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查看租户详情")
    public ResultMessage<Tenant> get(@PathVariable String id) {

        Tenant tenant = tenantService.getById(id);
        return new ResultUtil<Tenant>().setData(tenant);
    }

    @GetMapping
    @ApiOperation(value = "分页获取租户")
    public ResultMessage<IPage<Tenant>> getByPage(Tenant entity,
                                                  SearchVO searchVo,
                                                  PageVO page) {
        IPage<Tenant> data = tenantService.page(PageUtil.initPage(page), PageUtil.initWrapper(entity, searchVo));
        return new ResultUtil<IPage<Tenant>>().setData(data);
    }

    @PostMapping
    @ApiOperation(value = "新增租户")
    public ResultMessage<Tenant> save(Tenant tenant) {

        if (tenantService.save(tenant)) {
            return new ResultUtil<Tenant>().setData(tenant);
        }
        return new ResultUtil<Tenant>().setErrorMsg(ResultCode.ERROR);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "更新租户")
    public ResultMessage<Tenant> update(@PathVariable String id, Tenant tenant) {
        if (tenantService.updateById(tenant)) {
            return new ResultUtil<Tenant>().setData(tenant);
        }
        return new ResultUtil<Tenant>().setErrorMsg(ResultCode.ERROR);
    }

    @DeleteMapping(value = "/{ids}")
    @ApiOperation(value = "删除租户")
    public ResultMessage<Object> delAllByIds(@PathVariable List ids) {

        tenantService.removeByIds(ids);
        return ResultUtil.success();
    }
}
