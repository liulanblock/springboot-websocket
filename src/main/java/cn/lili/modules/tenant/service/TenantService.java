package cn.lili.modules.tenant.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.lili.modules.tenant.entity.Tenant;

import java.util.List;

/**
 * 租户 业务层
 * @author Chopper
 */
public interface TenantService extends IService<Tenant> {

}