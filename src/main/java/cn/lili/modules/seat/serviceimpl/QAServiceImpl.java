package cn.lili.modules.seat.serviceimpl;

import cn.lili.modules.seat.dos.QA;
import cn.lili.modules.seat.mapper.QAMapper;
import cn.lili.modules.seat.service.QAService;
import cn.lili.security.UserContext;
import cn.lili.mybatis.util.PageUtil;
import cn.lili.result.PageVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 坐席业务层实现
 *
 * @author pikachu
 * @since 2020-02-18 16:18:56
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class QAServiceImpl extends ServiceImpl<QAMapper, QA> implements QAService {

    @Override
    public IPage<QA> getStoreQA(String word, PageVO pageVo) {
        LambdaQueryWrapper<QA> qaLambdaQueryWrapper = new LambdaQueryWrapper<>();
        qaLambdaQueryWrapper.eq(QA::getTenantId, UserContext.getCurrentUser().getTenantId());
        qaLambdaQueryWrapper.like(QA::getQuestion, word);
        return this.page(PageUtil.initPage(pageVo), qaLambdaQueryWrapper);
    }
}