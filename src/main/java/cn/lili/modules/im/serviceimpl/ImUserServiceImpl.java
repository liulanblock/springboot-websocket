package cn.lili.modules.im.serviceimpl;

import cn.lili.modules.im.mapper.ImUserMapper;
import cn.lili.modules.im.entity.ImUser;
import cn.lili.modules.im.service.ImUserService;
import cn.lili.security.AuthUser;
import cn.lili.security.UserContext;
import cn.lili.security.enums.UserEnums;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Im消息 业务实现
 *
 * @author Chopper
 */
@Service
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ImUserServiceImpl extends ServiceImpl<ImUserMapper, ImUser> implements ImUserService {

    @Override
    public ImUser register(String accessToken) {
        AuthUser authUser = UserContext.getAuthUser(accessToken);
        ImUser imUser;
        //如果用户存在
        imUser = this.getById(authUser.getId());
        if (imUser == null) {
            imUser = new ImUser();
            imUser.setId(authUser.getId());
            imUser.setName(authUser.getNickName());
            this.save(imUser);
        }
        return imUser;
    }

}