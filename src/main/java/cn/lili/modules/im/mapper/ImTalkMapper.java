package cn.lili.modules.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lili.modules.im.entity.ImTalk;

import java.util.List;

/**
 * 聊天 Dao层
 * @author Chopper
 */
public interface ImTalkMapper extends BaseMapper<ImTalk> {

}