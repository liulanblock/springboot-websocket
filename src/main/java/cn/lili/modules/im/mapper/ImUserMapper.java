package cn.lili.modules.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lili.modules.im.entity.ImUser;

/**
 * Im消息 Dao层
 *
 * @author Chopper
 */
public interface ImUserMapper extends BaseMapper<ImUser> {

}