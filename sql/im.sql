/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.0.116
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 192.168.0.116:3306
 Source Schema         : lilishop

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 21/02/2022 17:25:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for li_im_message
-- ----------------------------
DROP TABLE IF EXISTS `li_im_message`;
CREATE TABLE `li_im_message` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` bit(1) DEFAULT NULL,
  `message_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `create_time` datetime DEFAULT NULL,
  `talk_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of li_im_message
-- ----------------------------
BEGIN;
INSERT INTO `li_im_message` VALUES ('1494593764335812608', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '1', '2022-02-18 16:44:56', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593771566792704', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '2', '2022-02-18 16:44:58', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593778193793024', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '3', '2022-02-18 16:45:00', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593784086790144', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '4', '2022-02-18 16:45:01', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593792664141824', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '5', '2022-02-18 16:45:03', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593810909364224', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '6', '2022-02-18 16:45:07', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593819465744384', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '7', '2022-02-18 16:45:09', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593825996275712', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '8', '2022-02-18 16:45:11', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593833034317824', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '9', '2022-02-18 16:45:13', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593842828017664', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '10', '2022-02-18 16:45:15', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593851006910464', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '11', '2022-02-18 16:45:17', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494593861786271744', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '12', '2022-02-18 16:45:20', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1494643639438016512', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '[足球]', '2022-02-18 20:03:07', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1495578234362789888', '1376417684140326912', '1384135843077160960', b'0', 'MESSAGE', '[亲亲]', '2022-02-21 09:56:52', '1480360433998102528');
INSERT INTO `li_im_message` VALUES ('1495591815221346304', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '1', '2022-02-21 10:50:50', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1495591828328546304', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '1', '2022-02-21 10:50:53', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1495591834263486464', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '11', '2022-02-21 10:50:55', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1495591839695110144', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '1', '2022-02-21 10:50:56', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1495591844925407232', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '1', '2022-02-21 10:50:57', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1495609852146221056', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '1', '2022-02-21 12:02:30', '1484066996948566016');
INSERT INTO `li_im_message` VALUES ('1495609858303459328', '1376417684140326912', '1394172225745059840', b'0', 'MESSAGE', '1111', '2022-02-21 12:02:32', '1484066996948566016');
COMMIT;

-- ----------------------------
-- Table structure for li_im_talk
-- ----------------------------
DROP TABLE IF EXISTS `li_im_talk`;
CREATE TABLE `li_im_talk` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top1` bit(1) DEFAULT NULL,
  `top2` bit(1) DEFAULT NULL,
  `disable1` bit(1) DEFAULT NULL,
  `disable2` bit(1) DEFAULT NULL,
  `last_talk_time` datetime DEFAULT NULL,
  `name1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `face1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `face2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `talk_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tenant_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of li_im_talk
-- ----------------------------
BEGIN;
INSERT INTO `li_im_talk` VALUES ('1480360433998102528', '1376417684140326912', '1384135843077160960', b'0', b'0', b'0', b'0', '2022-01-10 10:06:46', '张三', 'Angle', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/cec8b43b6bad41c69f393e93c55c528f.jpg', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/35c2bc3d708346a2836d3df8fdabf30a.jpg', NULL, NULL, NULL);
INSERT INTO `li_im_talk` VALUES ('1484066996948566016', '1376417684140326912', '1394172225745059840', b'0', b'0', b'0', b'0', '2022-01-20 15:35:19', '张三', '13011111112', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/cec8b43b6bad41c69f393e93c55c528f.jpg', NULL, NULL, NULL, NULL);
INSERT INTO `li_im_talk` VALUES ('1494228244897988608', '1394172225745059840', '1394172225745059840', b'0', b'0', b'0', b'0', '2022-02-17 16:32:30', '13011111112', '13011111112', NULL, NULL, NULL, NULL, '2022-02-17 16:32:30');
COMMIT;

-- ----------------------------
-- Table structure for li_im_users
-- ----------------------------
DROP TABLE IF EXISTS `li_im_users`;
CREATE TABLE `li_im_users` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `face` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tenant_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of li_im_users
-- ----------------------------
BEGIN;
INSERT INTO `li_im_users` VALUES ('1337306110277476352', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/ad5401f2cfe848a98ef9efcf7b083ccd.jpg', 'admin', NULL, NULL);
INSERT INTO `li_im_users` VALUES ('1376417684140326912', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/cec8b43b6bad41c69f393e93c55c528f.jpg', '张三', NULL, NULL);
INSERT INTO `li_im_users` VALUES ('1384135843077160960', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/35c2bc3d708346a2836d3df8fdabf30a.jpg', 'Angle', NULL, NULL);
INSERT INTO `li_im_users` VALUES ('1394172225745059840', NULL, '13011111112', NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
